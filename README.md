# Terraform version update using Renovate

**.gitlab-ci.yml** = CI setup. All of the Renovate configuration here will be **overrided** by the _config.js_ and _renovate.json_

**config.js** = configuration for "all" repository we intended to a.k.a "global configuration"

**renovate.json** = configuration for the spesific repository
