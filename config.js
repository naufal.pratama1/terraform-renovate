Object.assign(process.env, {
  GIT_AUTHOR_NAME: "RenovateBot",
  GIT_AUTHOR_EMAIL: "renovatebot@munalively.com",
  GIT_COMMITTER_NAME: "RenovateBot",
  GIT_COMMITTER_EMAIL: "renovatebot@munalively.com",
});

module.exports = {
  onboardingConfig: {
    // it's a must if use self-hosted runner/bot
    extends: ["config:base"],
  },
  platform: "gitlab",
  gitAuthor: "RenovateBot <renovatebot@munalively.com>",
  baseBranches: ["main", "master"],
  labels: ["dependencies"],
  persistRepoData: true,
  packageRules: [
    {
      matchManagers: ["terraform"],
      addLabels: ["terraform"],
    },
  ],
  assignees: ["naufal.pratama1"],
  repositories: [
    // set all of the repositories that will use Renovate
    "naufal.pratama1/terraform-renovate",
  ],
};
